SELECT * FROM UCS;
SELECT * FROM CURSO;


CREATE TABLE curso (
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
NOME varchar(200) NOT NULL   
);

INSERT INTO CURSO (ID , NOME) VALUES (default, 'Tecnico_em logistica' );
insert into curso (id, nome) values (default, 'Administracao');
insert into curso (id, nome) values (default, 'seguranca do trabalho');


create table UCs 
(id int not null auto_increment primary key,
codigo varchar(20) ,
descricao varchar(200));

insert into ucs (codigo , descricao) values ('UC01', 'Organizar estoques');
insert into ucs (codigo , descricao) values ('UC02', 'monitorar processos');
insert into ucs (codigo , descricao) values ('UC03', 'controlar transporte');
insert into ucs (codigo , descricao) values ('UC01', 'planejamento estratégico');
insert into ucs (codigo , descricao) values ('UC02', 'operacionalizacão de projetos');
insert into ucs (codigo , descricao) values ('UC03', 'Auxiliar as operacões');
insert into ucs (codigo , descricao) values ('UC01', 'implementar PPS ');
insert into ucs (codigo , descricao) values ('UC02', 'medidas de controle');
insert into ucs (codigo , descricao) values ('UC03', 'ações educativas em saúde');

alter table UCs add column id_curso int not null;

update UCS SET ID_CURSO = (1) WHERE ID = 1;
update UCS SET ID_CURSO = (1) WHERE ID = 2;
update UCS SET ID_CURSO = (1) WHERE ID = 3;
update UCS SET ID_CURSO = (2) WHERE ID = 4;
update UCS SET ID_CURSO = (2) WHERE ID = 5;
update UCS SET ID_CURSO = (2) WHERE ID = 6;
update UCS SET ID_CURSO = (3) WHERE ID = 7;
update UCS SET ID_CURSO = (3) WHERE ID = 8;
update UCS SET ID_CURSO = (3) WHERE ID = 9;



CREATE TABLE IF NOT EXISTS Professor ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
NOME VARCHAR(200) NOT NULL 
) ;

INSERT INTO Professor (NOME) values ('Daniel Santiago') ; 
INSERT INTO Professor (NOME) values ('Thiago Portugal') ; 
INSERT INTO Professor (NOME) values ('Marcelo Ferreira') ; 
INSERT INTO Professor (NOME) values ('Marcia Fortuna') ; 
INSERT INTO Professor (NOME) values ('Jonatan Silva') ; 
INSERT INTO Professor (NOME) values ('Patrick Cansado') ; 
INSERT INTO professor (NOME) VALUES ('Eliana Belizario');
INSERT INTO professor (NOME) VALUES ('Andrea Costa ');
INSERT INTO professor (NOME) VALUES ('Eliana Belizario');
INSERT INTO professor (NOME) VALUES ('Valesca Silva');
INSERT INTO professor (NOME) VALUES ('Marcelo Santos');
INSERT INTO professor (NOME) VALUES ('Igor Gomes');
INSERT INTO professor (NOME) VALUES ('Jonas Silva');
INSERT INTO professor (NOME) VALUES ('Miguel Arcanjo');
INSERT INTO professor (NOME) VALUES ('Isaias da Silva');


CREATE TABLE IF NOT EXISTS Professor_Disciplina ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
ID_PROFESSOR INT NOT NULL , 
ID_DISCIPLINA INT NOT NULL  
) ;

drop table professor_disciplina;
insert into professor_disciplina (id_professor, id_disciplina) values (9, 4);
insert into professor_disciplina (id_professor, id_disciplina) values (8, 7);
insert into professor_disciplina (id_professor, id_disciplina) values (8, 2);
insert into professor_disciplina (id_professor, id_disciplina) values (8, 3 );
insert into professor_disciplina (id_professor, id_disciplina) values (10, 1);
insert into professor_disciplina (id_professor, id_disciplina) values (10, 4);
insert into professor_disciplina (id_professor, id_disciplina) values (10, 8);
insert into professor_disciplina (id_professor, id_disciplina) values (11, 5);
insert into professor_disciplina (id_professor, id_disciplina) values (15, 6);
insert into professor_disciplina (id_professor, id_disciplina) values (11, 2);



alter table ucs 
add constraint fk_curso foreign key (id_curso) 
references curso (id);

alter table professor_disciplina
add constraint fk_professor foreign key (id_professor)
references professor (id);

alter table professor_disciplina
add constraint fk_disciplina foreign key (id_disciplina)
references disciplina (id); 


