

CREATE TABLE IF NOT EXISTS Disciplina ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
UC varchar(100)); 

INSERT INTO Disciplina values (default, 'INFORMATICA BASICA' );
INSERT INTO Disciplina values (default, 'ESTRUTURA DE UMA TORRE' );
INSERT INTO Disciplina values (default, 'FORMATANDO O COMPUTADOR' );
INSERT INTO Disciplina values (default, 'SOFTWARE X HARDWARE' );
INSERT INTO Disciplina values (default, 'PROJETO INTEGRADOR 1' );
INSERT INTO Disciplina values (default, 'INRODUCAO A REDES' );
INSERT INTO Disciplina values (default, 'TOPOLOGIA' );
INSERT INTO Disciplina values (default, 'PASSANDO CABOS' );
INSERT INTO Disciplina values (default, 'CONFIGURANDO ROTEADOR' );
INSERT INTO Disciplina values (default, 'PROJETO INTEGRADOR 2' );
INSERT INTO Disciplina values (default, 'INTRODUCAO A PROGRAMACAO' );
INSERT INTO Disciplina values (default, 'JAVA');

DROP TABLE DISCIPLINA;