create schema locadora;

create table if not exists Genero (
id int not null auto_increment primary key,
Nome varchar(200)
);

create table if not exists Ator(
id int not null auto_increment primary key,
Primeiro_Nome varchar(200) ,
Ultimo_Nome varchar(200),
Local_de_Nascimento varchar(200),
Data_de_Nascimento date
);

create table if not exists Diretor(
id int not null auto_increment primary key,
Nome varchar(200),
Primeiro_Nome varchar(200),
Ultimo_Nome varchar(200),
Local_de_Nascimento varchar(200),
Data_de_Nascimento date
);

create table if not exists Filme(
id int not null auto_increment primary key,
Titulo varchar(200),
Ano_de_Lancamento int,
Avaliacao int not null,
Duracao_Min INT NOT NULL
);

drop table filme;
create table if not exists Genero_Ator(
id INT NOT NULL auto_increment key,
id_genero INT NOT NULL,
id_ator INT NOT NULL
);

create table if not exists Genero_Diretor(
id INT NOT NULL auto_increment key,
id_genero INT NOT NULL,
id_diretor INT NOT NULL
);

create table if not exists Genero_Filme(
id INT NOT NULL auto_increment key,
id_genero INT NOT NULL,
id_filme INT NOT NULL
);


create table if not exists Ator_Diretor(
id INT NOT NULL auto_increment key,
id_ator INT NOT NULL,
id_diretor INT NOT NULL
);


create table if not exists Ator_Filme(
id INT NOT NULL auto_increment key,
id_ator INT NOT NULL,
id_filme INT NOT NULL
);

create table if not exists Filme_Diretor(
id INT NOT NULL auto_increment key,
id_filme INT NOT NULL,
id_diretor INT NOT NULL
);


SELECT * FROM DIRETOR;
insert into filme (Titulo, ano_de_lancamento,avaliacao,duracao_min) values ('Your Name' , 2016, 9 , 106);
insert into genero (nome) value ('Drama Romantico');
insert into genero (nome) value ('Fantasia Cientifica');
INSERT INTO diretor (PRIMEIRO_NOME, ULTIMO_NOME, LOCAL_DE_NASCIMENTO, DATA_DE_NASCIMENTO) VALUES ('Quentin', 'Tarantino', 'EUA', '1963-03-27');
INSERT INTO diretor (PRIMEIRO_NOME, ULTIMO_NOME, LOCAL_DE_NASCIMENTO, DATA_DE_NASCIMENTO) VALUES ('Jose', 'Padilha', 'Brasil', '1967-08-01');
INSERT INTO diretor (PRIMEIRO_NOME, ULTIMO_NOME, LOCAL_DE_NASCIMENTO, DATA_DE_NASCIMENTO) VALUES ('Joon-Ho', 'Bong', 'Coreia do Sul', '1969-09-14');
INSERT INTO diretor (PRIMEIRO_NOME, ULTIMO_NOME, LOCAL_DE_NASCIMENTO, DATA_DE_NASCIMENTO) VALUES ('Shawn ', 'Levy', 'Canada', '1968-07-23');
INSERT INTO diretor (PRIMEIRO_NOME, ULTIMO_NOME, LOCAL_DE_NASCIMENTO, DATA_DE_NASCIMENTO) VALUES ('Makoto', 'Niitsu', 'Japao', '1973-02-09');




