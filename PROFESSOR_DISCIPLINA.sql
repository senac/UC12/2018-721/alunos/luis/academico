CREATE TABLE IF NOT EXISTS PROFESSOR_DISCIPLINA ( 
ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY  , 
PROFESSOR varchar(100),
DISCIPLINA VARCHAR(100)
); 


INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL' , 'INFORMATICA BASICA' );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'ESTRUTURA DE UMA TORRE' );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'FORMATANDO O COMPUTADOR');
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'SOFTWARE X HARDWARE'  );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'PROJETO INTEGRADOR 1'  );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL' , 'INRODUCAO A REDES' );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'TOPOLOGIA');
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL' , 'PASSANDO CABOS' );
INSERT INTO Disciplina values (default , 'THIAGO PORTUGAL', 'CONFIGURANDO ROTEADOR');
INSERT INTO Disciplina values (default, 'THIAGO PORTUGAL', 'PROJETO INTEGRADOR 2' );
INSERT INTO Disciplina values (default, 'DANIEL SANTIAGO', 'INTRODUCAO A PROGRAMACAO');
INSERT INTO Disciplina values (default, 'DANIEL SANTIAGO', 'JAVA');

DROP TABLE PROFESSOR_DISCIPLINA;